package vip.learning.gateway.factory;

import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import vip.learning.gateway.config.VipTimeBetweenConfig;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @ClassName VipTimeBetweenRoutePredicateFactory
 * @Description
 * @Author hubin
 * @Date 2022/1/6 22:46
 * @Version V1.0
 **/
@Component
public class VipTimeBetweenRoutePredicateFactory extends AbstractRoutePredicateFactory<VipTimeBetweenConfig> {


    public VipTimeBetweenRoutePredicateFactory() {
        super(VipTimeBetweenConfig.class);
    }

    // 真正业务逻辑所在位置
    @Override
    public Predicate<ServerWebExchange> apply(VipTimeBetweenConfig config) {

        ZonedDateTime startTime = config.getStartTime();
        ZonedDateTime endTime = config.getEndTime();
        return new Predicate<ServerWebExchange>() {
            @Override
            public boolean test(ServerWebExchange serverWebExchange) {

                // 判断当前时间是否在配置时间范围类
                ZonedDateTime now = ZonedDateTime.now();
                return now.isAfter(startTime) && now.isBefore(endTime);
            }
        };
    }


    // 用于接收时间参数
    public List<String> shortcutFieldOrder(){
        return Arrays.asList("startTime","endTime");
    }

}

