package vip.learning.gateway.config;


import java.time.ZonedDateTime;

/**
 * @ClassName VipTimeBetweenConfig
 * @Description
 * @Author hubin
 * @Date 2022/1/6 22:47
 * @Version V1.0
 **/
public class VipTimeBetweenConfig {

    private ZonedDateTime startTime;
    private ZonedDateTime endTime;

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public ZonedDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }
}

