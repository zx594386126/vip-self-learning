package vip.learning.order.rpc.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @ClassName FeignLogConfig
 * @Description
 * @Author hubin
 * @Date 2022/1/11 21:53
 * @Version V1.0
 **/
//@Configuration
public class FeignLogConfig {

    //@Bean
    Logger.Level feignLog(){
        return Logger.Level.FULL;
    }

}

