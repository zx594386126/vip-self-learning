package vip.learning.order.rpc.entity;

import lombok.Data;

import javax.persistence.Table;

/**
 * @ClassName StorageDO
 * @Description
 * @Author hubin
 * @Date 2022/1/26 16:54
 * @Version V1.0
 **/
@Data
@Table(name = "tb_storage")
public class StorageDO {

    private Integer id;

    private String commodityCode;

    private Integer count;


}

