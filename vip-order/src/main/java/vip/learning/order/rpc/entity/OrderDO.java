package vip.learning.order.rpc.entity;

import lombok.Data;

import javax.persistence.Table;

/**
 * @ClassName OrderDO
 * @Description
 * @Author hubin
 * @Date 2022/1/26 16:53
 * @Version V1.0
 **/
@Data
@Table(name = "tb_order")
public class OrderDO {


    private Integer id;

    private  String userId;

    private String commodityCode;

    private Integer count;

    private Integer money;

}

