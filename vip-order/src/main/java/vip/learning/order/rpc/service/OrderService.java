package vip.learning.order.rpc.service;

import vip.learning.order.rpc.rest.vo.OrderVo;
import vip.learning.order.rpc.utils.HttpResult;

import java.math.BigDecimal;

/**
 * @ClassName OrderService
 * @Description
 * @Author hubin
 * @Date 2021/1/27 17:39
 * @Version V1.0
 **/
public interface OrderService {

    public void backendServer();


    OrderVo createOrder(String userId, String commodityCode, Integer count);
}
