package vip.learning.order.rpc.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import vip.learning.order.rpc.config.FeignLogConfig;
import vip.learning.order.rpc.fallback.ProductFeignClientFallBack;

/**
 * @ClassName ProductFeignClient
 * @Description
 * @Author hubin
 * @Date 2022/1/11 21:49
 * @Version V1.0
 **/
@FeignClient(name = "vip-ware")
public interface WareFeignClient {

    @PostMapping(value = "/ware/deduct")
    public String dedubt(@RequestParam("commodityCode") String commodityCode,@RequestParam("count") int count);
}
