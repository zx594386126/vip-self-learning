package vip.learning.order.rpc.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import vip.learning.order.rpc.config.FeignLogConfig;
import vip.learning.order.rpc.fallback.ProductFeignClientFallBack;

/**
 * @ClassName ProductFeignClient
 * @Description
 * @Author hubin
 * @Date 2022/1/11 21:49
 * @Version V1.0
 **/
@FeignClient(name = "vip-product",configuration = FeignLogConfig.class,fallback = ProductFeignClientFallBack.class)
public interface ProductFeignClient {

    @RequestMapping("/product/get/pro")
    public String getPro();
}
