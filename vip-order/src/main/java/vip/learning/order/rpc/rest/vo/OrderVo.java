package vip.learning.order.rpc.rest.vo;

import lombok.Data;

/**
 * @ClassName OrderVo
 * @Description
 * @Author hubin
 * @Date 2022/1/26 17:14
 * @Version V1.0
 **/
@Data
public class OrderVo {


    private Integer id;

    private  String userId;

    private String commodityCode;

    private Integer count;

    private Integer money;

}

