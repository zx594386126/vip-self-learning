package vip.learning.order.rpc.mapper;

import tk.mybatis.mapper.common.Mapper;
import vip.learning.order.rpc.entity.StorageDO;

/**
 * @ClassName StorageMapper
 * @Description
 * @Author hubin
 * @Date 2022/1/26 17:11
 * @Version V1.0
 **/
@org.apache.ibatis.annotations.Mapper
public interface StorageMapper extends Mapper<StorageDO> {
}
