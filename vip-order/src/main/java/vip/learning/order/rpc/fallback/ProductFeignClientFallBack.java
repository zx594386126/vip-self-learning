package vip.learning.order.rpc.fallback;

import org.springframework.stereotype.Component;
import vip.learning.order.rpc.feign.ProductFeignClient;

/**
 * @ClassName ProductFeignClientFallBack
 * @Description
 * @Author hubin
 * @Date 2022/1/13 22:46
 * @Version V1.0
 **/
@Component
public class ProductFeignClientFallBack implements ProductFeignClient {
    @Override
    public String getPro() {
        return "降级了，或者限流了!";
    }
}

