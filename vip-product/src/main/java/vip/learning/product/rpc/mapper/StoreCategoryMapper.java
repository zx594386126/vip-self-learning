package vip.learning.product.rpc.mapper;

import tk.mybatis.mapper.common.Mapper;
import vip.learning.product.rpc.entity.StoreCategory;

/**
 * @ClassName StoreCategoryMapper
 * @Description
 * @Author hubin
 * @Date 2021/2/4 14:55
 * @Version V1.0
 **/
@org.apache.ibatis.annotations.Mapper
public interface StoreCategoryMapper extends Mapper<StoreCategory>{
}

