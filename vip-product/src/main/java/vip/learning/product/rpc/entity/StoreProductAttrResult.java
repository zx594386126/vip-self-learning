/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.kaikeba.co

 */
package vip.learning.product.rpc.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author jack胡
 */
@Data
@Table(name = "store_product_attr_result")
public class StoreProductAttrResult implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    /** 商品ID */
    @Column(name = "product_id")
    private Integer productId;


    /** 商品属性参数 */
    private String result;


    /** 上次修改时间 */
    private Integer changeTime;


    public void copy(StoreProductAttrResult source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
