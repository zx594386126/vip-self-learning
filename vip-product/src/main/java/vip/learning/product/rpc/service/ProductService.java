package vip.learning.product.rpc.service;

import vip.learning.product.rpc.entity.StoreProduct;
import vip.learning.product.rpc.utils.HttpResult;

import java.math.BigDecimal;

/**
 * @ClassName ProductService
 * @Description
 * @Author hubin
 * @Date 2021/1/27 17:39
 * @Version V1.0
 **/
public interface ProductService {


    HttpResult findOne(Long id);

    /**
     * @Description: 根据id查询商品信息
     * @Author: hubin
     * @CreateDate: 2021/2/4 16:52
     * @UpdateUser: hubin
     * @UpdateDate: 2021/2/4 16:52
     * @UpdateRemark: 修改内容
     * @Version: 1.0
     */
    StoreProduct findStoreProductById(Long skuId);

    /**
     * @Description: 根据id查询商品价格
     * @Author: hubin
     * @CreateDate: 2021/2/16 11:12
     * @UpdateUser: hubin
     * @UpdateDate: 2021/2/16 11:12
     * @UpdateRemark: 修改内容
     * @Version: 1.0
     */
    BigDecimal getPrice(Integer skuId);


    public void backendServer();
}
