package vip.learning.product.rpc.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vip.learning.product.rpc.entity.StoreProduct;
import vip.learning.product.rpc.mapper.StoreProductMapper;
import vip.learning.product.rpc.service.ProductService;
import vip.learning.product.rpc.utils.HttpResult;

import java.math.BigDecimal;

/**
 * @ClassName ProductServiceImpl
 * @Description
 * @Author hubin
 * @Date 2021/1/27 17:40
 * @Version V1.0
 **/
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private StoreProductMapper storeProductMapper;

    @Override
    public HttpResult findOne(Long id) {

        //查询商品
        StoreProduct storeProduct = storeProductMapper.selectByPrimaryKey(id);

        return HttpResult.ok(storeProduct);
    }

    /**
     * @Description: 根据id查询商品信息
     * @Author: hubin
     * @CreateDate: 2021/2/4 16:52
     * @UpdateUser: hubin
     * @UpdateDate: 2021/2/4 16:52
     * @UpdateRemark: 修改内容;
     * @Version: 1.0
     */
    @Override
    public StoreProduct findStoreProductById(Long skuId) {
        //查询商品
        StoreProduct storeProduct = storeProductMapper.selectByPrimaryKey(skuId);
        return storeProduct;
    }

    /**
     * @Description: 根据id查询商品价格
     * @Author: hubin
     * @CreateDate: 2021/2/16 11:13
     * @UpdateUser: hubin
     * @UpdateDate: 2021/2/16 11:13
     * @UpdateRemark: 修改内容
     * @Version: 1.0
     */
    @Override
    public BigDecimal getPrice(Integer skuId) {

        // 查询商品价格
        StoreProduct storeProduct = storeProductMapper.selectByPrimaryKey(skuId);
        return storeProduct.getPrice();
    }


    public void backendServer(){

        System.out.println("service....");

    }





}

