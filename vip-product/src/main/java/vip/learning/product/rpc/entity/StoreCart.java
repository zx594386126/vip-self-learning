/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.kaikeba.co

 */
package vip.learning.product.rpc.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author jack胡
 */

@Data
@Table(name = "store_cart")
public class StoreCart implements Serializable {

    /** 购物车表ID */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /** 用户ID */
    private Integer uid;


    /** 类型 */
    private String type;


    /** 商品ID */
    private Integer productId;


    /** 商品属性 */
    private String productAttrUnique;


    /** 商品数量 */
    private Integer cartNum;


    /** 添加时间 */
    private Integer addTime;


    /** 0 = 未购买 1 = 已购买 */
    private Integer isPay;


    /** 是否删除 */
    private Integer isDel;


    /** 是否为立即购买 */
    private Integer isNew;


    /** 拼团id */
    //@Column(name = "combination_id")
    private Integer combinationId;


    /** 秒杀产品ID */
    private Integer seckillId;


    /** 砍价id */
    private Integer bargainId;


    public void copy(StoreCart source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
