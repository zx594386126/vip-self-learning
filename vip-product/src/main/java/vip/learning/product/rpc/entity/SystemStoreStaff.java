/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.kaikeba.co

 */
package vip.learning.product.rpc.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author jack胡
 */
@Data
@Table(name = "system_store_staff")
public class SystemStoreStaff implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    /** 微信用户id */
    private Integer uid;


    /** 店员头像 */
    @NotBlank(message = "请选择用户")
    private String avatar;


    /** 门店id */
    @NotNull(message = "请选择门店")
    private Integer storeId;


    /** 店员名称 */
    @NotBlank(message = "请输入店员名称")
    private String staffName;


    /** 手机号码 */
    @NotBlank(message = "请输入手机号码")
    private String phone;


    /** 核销开关 */
    private Integer verifyStatus;


    /** 状态 */
    private Integer status;


    /** 添加时间 */
    private Integer addTime;


    /** 微信昵称 */
    private String nickname;


    /** 所属门店 */
    private String storeName;


    public void copy(SystemStoreStaff source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
