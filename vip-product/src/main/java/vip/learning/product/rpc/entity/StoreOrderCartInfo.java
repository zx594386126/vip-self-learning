/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.kaikeba.co

 */
package vip.learning.product.rpc.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author jack胡
 */

@Data
@Table(name = "store_order_cart_info")
public class StoreOrderCartInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    /** 订单id */
    private Integer oid;


    /** 购物车id */
    private Integer cartId;


    /** 商品ID */
    private Integer productId;


    /** 购买东西的详细信息 */
    private String cartInfo;


    /** 唯一id */
    private String unique;


    public void copy(StoreOrderCartInfo source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
