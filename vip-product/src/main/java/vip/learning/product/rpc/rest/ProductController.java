package vip.learning.product.rpc.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vip.learning.product.rpc.entity.StoreProduct;
import vip.learning.product.rpc.service.ProductService;
import vip.learning.product.rpc.utils.HttpResult;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;


/**
 * @Description:
 * @Created: description
 * @author: hubin
 * @createTime: 2020-06-19 11:21
 **/

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/product/rpc/{id}")
    public HttpResult findOne(@PathVariable Long id){

        //调用服务端
        HttpResult result = productService.findOne(id);

        return result;

    }

    /**
     * @Description: 根据id查询商品信息
     * @Author: hubin
     * @CreateDate: 2021/2/4 16:52
     * @UpdateUser: hubin
     * @UpdateDate: 2021/2/4 16:52
     * @UpdateRemark: 修改内容
     * @Version: 1.0
     */
    @RequestMapping("/sku/{skuId}")
    StoreProduct findStoreProductById(@PathVariable("skuId") Long skuId){
        // 查询商品信息
        StoreProduct product = productService.findStoreProductById(skuId);
        return product;
    }

    @RequestMapping("/price/{skuId}")
    BigDecimal getPrice(@PathVariable("skuId") Integer skuId){
        //查询商品价格
        BigDecimal price = productService.getPrice(skuId);
        return price;
    }


    @RequestMapping("/test-a")
    public String testA(){
        productService.backendServer();
        return "test-a";
    }

    @RequestMapping("/test-b")
    public String testB(){
        productService.backendServer();
        return "test-b";
    }


    @RequestMapping("/test-hot")
    public String testHot(@RequestParam(required = false) String a ,
                          @RequestParam(required = false) String b,
                          HttpServletRequest request){

        String url = request.getRequestURI();
        System.out.println("请求地址是："+url);
        return a + "test-b" + b;
    }


    @RequestMapping("/get/pro")
    public String getPro(){
        int i = 10/0;
        return "rpc is success!";
    }

}