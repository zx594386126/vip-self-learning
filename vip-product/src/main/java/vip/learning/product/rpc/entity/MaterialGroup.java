/**
 * Copyright (C) 2018-2020
 * All rights reserved, Designed By www.kaikeba.co

 */
package vip.learning.product.rpc.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author jack胡
 */
@Data
@Table(name="material_group")
public class MaterialGroup implements Serializable {

    /** PK */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    /** 逻辑删除标记（0：显示；1：隐藏） */
    private Boolean delFlag;


    /** 创建时间 */
    private Timestamp createTime;

    /** 创建者ID */
    private String createId;


    /** 分组名 */
    private String name;


    public void copy(MaterialGroup source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
