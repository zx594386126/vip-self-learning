package vip.learning.ware.service;

import vip.learning.ware.web.vo.StorageVo;

/**
 * @ClassName WareService
 * @Description
 * @Author hubin
 * @Date 2022/1/27 14:31
 * @Version V1.0
 **/
public interface WareService {
    /**
     * 扣除存储数量
     */
    void deduct(String commodityCode, int count);

    /**
     * @Description: 根据商品唯一code查询库存
     * @Author: hubin
     * @CreateDate: 2022/1/27 14:42
     * @UpdateUser: hubin
     * @UpdateDate: 2022/1/27 14:42
     * @UpdateRemark: 修改内容
     * @Version: 1.0
     */
    StorageVo findOne(String commodityCode);
}

