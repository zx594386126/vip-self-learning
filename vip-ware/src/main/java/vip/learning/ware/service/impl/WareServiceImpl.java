package vip.learning.ware.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.learning.ware.entity.StorageDO;
import vip.learning.ware.mapper.WareMapper;
import vip.learning.ware.service.WareService;
import vip.learning.ware.web.vo.StorageVo;

/**
 * @ClassName WareServiceImpl
 * @Description
 * @Author hubin
 * @Date 2022/1/27 14:34
 * @Version V1.0
 **/
@Service
@Slf4j
public class WareServiceImpl implements WareService {

    @Autowired
    private WareMapper wareMapper;
    /**
     * 扣除存储数量
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deduct(String commodityCode, int count) {
            int deduct = wareMapper.deduct(commodityCode, count);
    }

    @Override
    public StorageVo findOne(String commodityCode) {
        StorageDO storageDO = wareMapper.findOne(commodityCode);
        StorageVo storageVo = new StorageVo();
        BeanUtils.copyProperties(storageDO,storageVo);
        return storageVo;
    }
}

