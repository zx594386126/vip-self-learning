package vip.learning.ware.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;
import vip.learning.ware.entity.StorageDO;

/**
 * @ClassName WareMapper
 * @Description
 * @Author hubin
 * @Date 2022/1/27 14:32
 * @Version V1.0
 **/
@org.apache.ibatis.annotations.Mapper
public interface WareMapper extends Mapper<StorageDO> {

    /**
     * 扣除存储数量
     */
    @Update(value = "update tb_storage set count = count - #{count} where commodity_code = #{commodityCode}")
    public int deduct(@Param("commodityCode") String commodityCode, @Param("count") int count);

    @Select(value = "select * from tb_storage where commodity_code = #{commodityCode}")
    StorageDO findOne(String commodityCode);
}
