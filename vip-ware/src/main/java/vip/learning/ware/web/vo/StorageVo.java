package vip.learning.ware.web.vo;

import lombok.Data;

/**
 * @ClassName StorageVo
 * @Description
 * @Author hubin
 * @Date 2022/1/27 14:41
 * @Version V1.0
 **/
@Data
public class StorageVo {

    private Integer id;

    private String commodityCode;

    private Integer count;
}

