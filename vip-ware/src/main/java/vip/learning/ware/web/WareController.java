package vip.learning.ware.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.learning.ware.service.WareService;
import vip.learning.ware.web.vo.StorageVo;

import java.util.concurrent.ConcurrentHashMap;


/**
 * @ClassName WareController
 * @Description
 * @Author hubin
 * @Date 2022/1/23 14:15
 * @Version V1.0
 **/
@RestController
@RequestMapping("/ware")
public class WareController {

    @Autowired
    private WareService wareService;

    @Value("${ware.name}")
    private String name;

    @Value("${ware.age}")
    private Integer age;

    @RequestMapping("/show/v1")
    public String showV1(){
        return "name: "+ name + " age: "+age;
    }

    @RequestMapping("/show/test")
    public StorageVo showTest(String commodityCode){
        StorageVo storageVo = wareService.findOne(commodityCode);
        return storageVo;
    }

    @PostMapping("deduct")
    public String dedubt(String commodityCode,int count){
        wareService.deduct(commodityCode,count);
        return "OK";
    }

}

