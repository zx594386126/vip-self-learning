package vip.learning.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName WareApplication
 * @Description
 * @Author hubin
 * @Date 2022/1/23 14:13
 * @Version V1.0
 **/
@EnableDiscoveryClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableFeignClients
public class WareApplication {

    public static void main(String[] args) {

            SpringApplication.run(WareApplication.class, args);

    }

}

