package vip.learning.account.entity;


import lombok.Data;

import javax.persistence.Table;

/**
 * @ClassName AccountDO
 * @Description
 * @Author hubin
 * @Date 2022/1/26 16:51
 * @Version V1.0
 **/
@Table(name = "tb_account")
@Data
public class AccountDO {

    private Integer id;

    private  String userId;

    private Integer money;

}

