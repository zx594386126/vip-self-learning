package vip.learning.account.web;

import cn.hutool.system.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.learning.account.service.AccountService;

/**
 * @ClassName AccountController
 * @Description
 * @Author hubin
 * @Date 2022/1/27 15:09
 * @Version V1.0
 **/
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/debit")
    public String debit(String userId,int money){
        accountService.debit(userId,money);
        return "ok";
    }
}

