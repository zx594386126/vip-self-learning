package vip.learning.account.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;
import vip.learning.account.entity.AccountDO;

/**
 * @ClassName AccountMapper
 * @Description
 * @Author hubin
 * @Date 2022/1/27 15:10
 * @Version V1.0
 **/
@org.apache.ibatis.annotations.Mapper
public interface AccountMapper extends Mapper<AccountDO> {

    @Update(value = "update tb_account set money=money - #{money} where user_id = #{userId}")
    void debit(@Param("userId") String userId, @Param("money") int money);
}
