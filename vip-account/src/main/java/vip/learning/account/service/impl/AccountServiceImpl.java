package vip.learning.account.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.learning.account.mapper.AccountMapper;
import vip.learning.account.service.AccountService;

/**
 * @ClassName AccountServiceImpl
 * @Description
 * @Author hubin
 * @Date 2022/1/27 15:12
 * @Version V1.0
 **/
@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    // 扣减账户金额
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void debit(String userId, int money) {
            accountMapper.debit(userId,money);
            // 模拟异常
            int i= 10/0;
    }
}

