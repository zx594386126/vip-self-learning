package vip.learning.account.service;

/**
 * @ClassName AccountService
 * @Description
 * @Author hubin
 * @Date 2022/1/27 15:10
 * @Version V1.0
 **/
public interface AccountService {

    /**
     * 从用户账户中扣除
     */
    void debit(String userId, int money);

}
